package com.example.nuestrohorcadovol3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imgHorca =(ImageView) findViewById(R.id.imgHorca); //Inicia apareciendo la imagen de la horca-->visible
        imgHorca.setVisibility(View.VISIBLE);
        Button btn = (Button) findViewById(R.id.btnCheck);  //Funcionalida al boton dandole click
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Casteo de los editText y busca al elemento a partir de su id
                EditText editL = (EditText) findViewById(R.id.editL);
                EditText editA = (EditText) findViewById(R.id.editA);
                EditText editY = (EditText) findViewById(R.id.editY);
                EditText editO = (EditText) findViewById(R.id.editO);
                EditText editU = (EditText) findViewById(R.id.editU);
                EditText editT = (EditText) findViewById(R.id.editT);
                EditText editPrinciapl = (EditText) findViewById(R.id.editPrincipal);

                int c=0; //declara variable contador --> c
               // while (c==5)

                        if (editPrinciapl.getText().toString().equals("l")) { //Condicion si lo ingresado en editPrincipal es diferente a la letra l
                            Toast.makeText(MainActivity.this, "¡Muy bien!", Toast.LENGTH_LONG).show(); //imprimir "¡Muy bien!"
                            editL.setText("l"); //imprime una l en editL
                        } else {
                            Toast.makeText(MainActivity.this, "ups, ¡Intenta de nuevo!", Toast.LENGTH_LONG).show(); //imprimir "ups, ¡Intenta de nuevo!"
                            c+=1; //Suma 1 a la variable c contador
                            contador(c); //Llama a la funcion contador y contiene la variable c
                        }
                        if (editPrinciapl.getText().toString().equals("a")) { //Condicion si lo ingresado en editPrincipal es diferente a la letra a
                            Toast.makeText(MainActivity.this, "¡Muy bien!", Toast.LENGTH_LONG).show(); //imprimir "¡Muy bien!"
                            editA.setText("a"); //imprime una a en editA

                        } else {
                            Toast.makeText(MainActivity.this, "ups, ¡Intenta de nuevo!", Toast.LENGTH_LONG).show(); //imprimir "ups, ¡Intenta de nuevo!"
                            c+=1; //Suma 1 a la variable c contador
                            contador(c); //Llama a la funcion contador y contiene la variable c
                        }
                        if (editPrinciapl.getText().toString().equals("y")) { //Condicion si lo ingresado en editPrincipal es diferente a la letra y
                            Toast.makeText(MainActivity.this, "¡Muy bien!", Toast.LENGTH_LONG).show(); //imprimir "¡Muy bien!"
                            editY.setText("y"); //imprime una y en editY

                        } else {
                            Toast.makeText(MainActivity.this, "ups, ¡Intenta de nuevo!", Toast.LENGTH_LONG).show(); //imprimir "ups, ¡Intenta de nuevo!"
                            c+=1; //Suma 1 a la variable c contador
                            contador(c); //Llama a la funcion contador y contiene la variable c
                        }
                        if (editPrinciapl.getText().toString().equals("o")) { //Condicion si lo ingresado en editPrincipal es diferente a la letra o
                            Toast.makeText(MainActivity.this, "¡Muy bien!", Toast.LENGTH_LONG).show(); //imprimir "¡Muy bien!"
                            editO.setText("o"); //imprime una o en editO

                        } else {
                            Toast.makeText(MainActivity.this, "ups, ¡Intenta de nuevo!", Toast.LENGTH_LONG).show(); //imprimir "ups, ¡Intenta de nuevo!"
                            c+=1; //Suma 1 a la variable c contador
                            contador(c); //Llama a la funcion contador y contiene la variable c
                        }
                        if (editPrinciapl.getText().toString().equals("u")) { //Condicion si lo ingresado en editPrincipal es diferente a la letra u
                            Toast.makeText(MainActivity.this, "¡Muy bien!", Toast.LENGTH_LONG).show(); //imprimir "¡Muy bien!"
                            editU.setText("u"); //imprime una u en editU

                        } else {
                            Toast.makeText(MainActivity.this, "ups, ¡Intenta de nuevo!", Toast.LENGTH_LONG).show(); //imprimir "ups, ¡Intenta de nuevo!"
                            c+=1; //Suma 1 a la variable c contador
                            contador(c); //Llama a la funcion contador y contiene la variable c
                        }
                        if (editPrinciapl.getText().toString().equals("t")) { //Condicion si lo ingresado en editPrincipal es diferente a la letra t
                            Toast.makeText(MainActivity.this, "¡Muy bien!", Toast.LENGTH_LONG).show(); //imprimir "¡Muy bien!"
                            editT.setText("t"); //imprime una t en editT

                        } else {
                            Toast.makeText(MainActivity.this, "ups, ¡Intenta de nuevo!", Toast.LENGTH_LONG).show(); //imprimir "ups, ¡Intenta de nuevo!"
                            c+=1; //Suma 1 a la variable c contador
                            contador(c); //Llama a la funcion contador y contiene la variable c
                        }
                        if(c==5){ //si la variable c es igual a 5
                            Toast.makeText(MainActivity.this, "ups, ¡Perdiste!", Toast.LENGTH_LONG).show(); //imprime "ups, ¡Perdiste!"
                        }
                        //entonces si los edit estan con las letras l,a,y,o,u,t
                        else if (editL.getText().toString().equals("l")&&editA.getText().toString().equals("a")&&editY.getText().toString().equals("y")&&editO.getText().toString().equals("o")&&editU.getText().toString().equals("u")&&editT.getText().toString().equals("t")){
                            Toast.makeText(MainActivity.this, "yei ¡Ganaste!", Toast.LENGTH_LONG).show();  //imprime "yei ¡Ganaste!"
                        }
              // ;

            }
        });
    }
    public void intento1 (){ //Funcion con nombre intento 1
        //Imagen horca invisible
        ImageView imgHorca =(ImageView) findViewById(R.id.imgHorca);
        imgHorca.setVisibility(View.INVISIBLE);
        //Imagen cuerpo visible
        ImageView imgCuerpo =(ImageView) findViewById(R.id.imgCuerpo);
        imgCuerpo.setVisibility(View.VISIBLE);
        //Imagen mano derecha invisible
        ImageView imgManoDere =(ImageView) findViewById(R.id.imgManoDere);
        imgManoDere.setVisibility(View.INVISIBLE);
        //Imagen mano izquierda invisible
        ImageView imgManoIzq =(ImageView) findViewById(R.id.imgManoIzq);
        imgManoIzq.setVisibility(View.INVISIBLE);
        //Imagen Pie izquierdo invisible
        ImageView imgPieIzquierdo =(ImageView) findViewById(R.id.imgPieIzquierdo);
        imgPieIzquierdo.setVisibility(View.INVISIBLE);
        //Imagen pie derecho invisible
        ImageView imgPieDerechoX =(ImageView) findViewById(R.id.imgPieDerechoX);
        imgPieDerechoX.setVisibility(View.INVISIBLE);
    }
    public void intento2 (){ //Funcion con nombre intento 2
        //Imagen horca invisible
        ImageView imgHorca =(ImageView) findViewById(R.id.imgHorca);
        imgHorca.setVisibility(View.INVISIBLE);
        //Imagen cuerpo invisible
        ImageView imgCuerpo =(ImageView) findViewById(R.id.imgCuerpo);
        imgCuerpo.setVisibility(View.INVISIBLE);
        //Imagen mano derecha visible
        ImageView imgManoDere =(ImageView) findViewById(R.id.imgManoDere);
        imgManoDere.setVisibility(View.VISIBLE);
        //Imagen mano izquierda invisible
        ImageView imgManoIzq =(ImageView) findViewById(R.id.imgManoIzq);
        imgManoIzq.setVisibility(View.INVISIBLE);
        //Imagen Pie izquierdo invisible
        ImageView imgPieIzquierdo =(ImageView) findViewById(R.id.imgPieIzquierdo);
        imgPieIzquierdo.setVisibility(View.INVISIBLE);
        //Imagen pie derecho invisible
        ImageView imgPieDerechoX =(ImageView) findViewById(R.id.imgPieDerechoX);
        imgPieDerechoX.setVisibility(View.INVISIBLE);
    }
    public void intento3 (){ //Funcion con nombre intento 3
        //Imagen horca invisible
        ImageView imgHorca =(ImageView) findViewById(R.id.imgHorca);
        imgHorca.setVisibility(View.INVISIBLE);
        //Imagen cuerpo invisible
        ImageView imgCuerpo =(ImageView) findViewById(R.id.imgCuerpo);
        imgCuerpo.setVisibility(View.INVISIBLE);
        //Imagen mano derecha visible
        ImageView imgManoDere =(ImageView) findViewById(R.id.imgManoDere);
        imgManoDere.setVisibility(View.INVISIBLE);
        //Imagen mano izquierda visible
        ImageView imgManoIzq =(ImageView) findViewById(R.id.imgManoIzq);
        imgManoIzq.setVisibility(View.VISIBLE);
        //Imagen Pie izquierdo invisible
        ImageView imgPieIzquierdo =(ImageView) findViewById(R.id.imgPieIzquierdo);
        imgPieIzquierdo.setVisibility(View.INVISIBLE);
        //Imagen pie derecho invisible
        ImageView imgPieDerechoX =(ImageView) findViewById(R.id.imgPieDerechoX);
        imgPieDerechoX.setVisibility(View.INVISIBLE);
    }
    public void intento4 (){ //Funcion con nombre intento 4
        //Imagen horca invisible
        ImageView imgHorca =(ImageView) findViewById(R.id.imgHorca);
        imgHorca.setVisibility(View.INVISIBLE);
        //Imagen cuerpo invisible
        ImageView imgCuerpo =(ImageView) findViewById(R.id.imgCuerpo);
        imgCuerpo.setVisibility(View.INVISIBLE);
        //Imagen mano derecha visible
        ImageView imgManoDere =(ImageView) findViewById(R.id.imgManoDere);
        imgManoDere.setVisibility(View.INVISIBLE);
        //Imagen mano izquierda invisible
        ImageView imgManoIzq =(ImageView) findViewById(R.id.imgManoIzq);
        imgManoIzq.setVisibility(View.INVISIBLE);
        //Imagen Pie izquierdo visible
        ImageView imgPieIzquierdo =(ImageView) findViewById(R.id.imgPieIzquierdo);
        imgPieIzquierdo.setVisibility(View.VISIBLE);
        //Imagen pie derecho invisible
        ImageView imgPieDerechoX =(ImageView) findViewById(R.id.imgPieDerechoX);
        imgPieDerechoX.setVisibility(View.INVISIBLE);
    }
    public void intento5 (){ //Funcion con nombre intento 5
        //Imagen horca invisible
        ImageView imgHorca =(ImageView) findViewById(R.id.imgHorca);
        imgHorca.setVisibility(View.INVISIBLE);
        //Imagen cuerpo invisible
        ImageView imgCuerpo =(ImageView) findViewById(R.id.imgCuerpo);
        imgCuerpo.setVisibility(View.INVISIBLE);
        //Imagen mano derecha visible
        ImageView imgManoDere =(ImageView) findViewById(R.id.imgManoDere);
        imgManoDere.setVisibility(View.INVISIBLE);
        //Imagen mano izquierda invisible
        ImageView imgManoIzq =(ImageView) findViewById(R.id.imgManoIzq);
        imgManoIzq.setVisibility(View.INVISIBLE);
        //Imagen Pie izquierdo invisible
        ImageView imgPieIzquierdo =(ImageView) findViewById(R.id.imgPieIzquierdo);
        imgPieIzquierdo.setVisibility(View.INVISIBLE);
        //Imagen pie derecho visible
        ImageView imgPieDerechoX =(ImageView) findViewById(R.id.imgPieDerechoX);
        imgPieDerechoX.setVisibility(View.VISIBLE);


    }
    public void contador(int c){ //Funcion con nombre contador y contiene la variable c
        if(c==1){ //Si c es igual a 1
            intento1(); //Llama a la funcion intento1
        }
        else if(c==2){ //Si c es igual a 2
            intento2(); //Llama a la funcion intento2
        }
        else if(c==3){ //Si c es igual a 3
            intento3(); //Llama a la funcion intento3
        }
        else if(c==4){ //Si c es igual a 4
            intento4(); //Llama a la funcion intento4
        }
        else if(c==5){ //Si c es igual a 5
            intento5(); //Llama a la funcion intento5
        }
    }
}